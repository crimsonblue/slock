/* user and group to drop privileges to */
static const char *user  = "nobody";
static const char *group = "nogroup";

static const char *colorname[NUMCOLS] = {
	[BACKGROUND] =   "#2e3440",     /* after initialization */
	[INIT] =   "#d8dee9",     /* after initialization */
	[INPUT] =  "#81a1c1",   /* during input */
	[FAILED] = "#bf616a",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 0;

/* time in seconds to cancel lock with mouse movement */
static const int timetocancel = 4;

/* default message */
static const char * message = "computer is locked. please enter password.";

/* text color */
static const char * text_color = "#d8dee9";

/* text size (must be a valid size) */
static const char * font_name = "Ubuntu Mono:size:pixelsize=48:antialias=true:autohint=true";

/* insert grid pattern with scale 1:1, the size can be changed with logosize */
static const int logosize = 64;
static const int logow = 12;	/* grid width and height for right center alignment*/
static const int logoh = 0;

static XRectangle rectangles[9] = {
	/* x	y	w	h */
	{ 0,	3,	1,	3 },
	{ 1,	3,	2,	1 },
	{ 0,	5,	8,	1 },
	{ 3,	0,	1,	5 },
	{ 5,	3,	1,	2 },
	{ 7,	3,	1,	2 },
	{ 8,	3,	4,	1 },
	{ 9,	4,	1,	2 },
	{ 11,	4,	1,	2 },

};
